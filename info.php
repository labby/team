<?php

/*

 Website Baker Project <http://www.websitebaker.org/>
 Copyright (C) 2004-2007, Ryan Djurovich

 Website Baker is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Website Baker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Website Baker; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

$module_directory = 'team';
$module_name = 'Team';
$module_function = 'page';
$module_version = '2.1.0';
$module_platform = '5.x';
$module_author = 'Ryan Djurovich, Bennie Wijs, Woudloper, Matthias Gallas, Niclas Brorsson, Chio Maisriml, erpe';
$module_license = 'GNU General Public License';
$module_description = 'This page type is designed for making a team/members page. It is based on the bookmarks module';
$module_guid = '5c80a071-2d12-4b18-8849-e740a53e42d2';

?>